/* global Package, Npm */

Package.describe({
  name: 'bmt:autoform-inputs',
  summary: 'AutoFormInputs',
  version: '1.2.5'
});

Npm.depends({
  'lodash': '4.17.4',
  'material-datetime-picker': '2.4.0',
  'autonumeric': '4.1.0-beta.16',
  'moment': '2.19.2',
  'inputmask': '3.3.11'
});

Package.onUse(function(api) {
  api.use([
    'check',
    'ecmascript',
    'underscore',
    'mongo',
    'blaze',
    'templating',
    'reactive-var',
    'tracker',
    'session',
  ]);

  api.addFiles([
    'client/inputs/inputs.html',
    'client/inputs/inputs.js',
    'client/inputs/datetimepicker.css',
  ], ['client']);

  /*api.mainModule('client/simpleList/simpleList.js', ['client']);*/

  api.export('modalArray');
});
