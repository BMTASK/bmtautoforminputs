/* eslint-disable import/no-extraneous-dependencies, import/no-unresolved, import/extensions */
import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { ReactiveField } from 'meteor/peerlibrary:reactive-field'
import { AutoForm } from 'meteor/aldeed:autoform'
import { Materialize } from 'meteor/materialize:materialize'
import { Template } from 'meteor/templating'
import { $ } from 'meteor/jquery'
/* eslint-enable */
import MaterialDateTimePicker from 'material-datetime-picker' // eslint-disable-line import/extensions
import AutoNumeric from 'autonumeric'
import moment from 'moment'
import _ from 'lodash'
import Inputmask from 'inputmask'

const refreshSelect = instance => {
    $(instance.findAll('select.proyecto-select')).materialSelect({
        label           : true,
        labelPosition   : 'top'
    })
}

Meteor.startup(() => {
    function linesToArray (text) {
        const lines = [],
            txt = text.split('\n')
        txt.forEach(line => {
            const ln = $.trim(line)
            if (ln.length) {
                lines.push(ln)
            }
        })
        return lines
    }

    AutoForm.addInputType('addImagenBlog', {
        template: 'agregarBlogImagen',
        valueOut () {
            if (this.context.files.length) {
                return true
            }
        }
    })

    AutoForm.addInputType('checkboxBlogImagen', {
        template: 'checkboxBlogImagen',
        valueOut () {
            return this.context.checked
        }
    })

    AutoForm.addInputType('textProyecto', {
        template: 'afProyectoInput',
        valueOut () {
            if (this[0].inputmask && this[0].inputmask.hasMaskedValue()) {
                if (!this[0].inputmask.isComplete()) {
                    return null
                }
                if (!this[0].inputmask.isNumber) {
                    return this[0].inputmask.unmaskedvalue()
                }
                return AutoForm.valueConverters.stringToNumber(this[0].inputmask.unmaskedvalue())
            }
            return this.val()
        },
        valueConverters: {
            stringArray     : AutoForm.valueConverters.stringToStringArray,
            number          : AutoForm.valueConverters.stringToNumber,
            numberArray     : AutoForm.valueConverters.stringToNumberArray,
            boolean         : AutoForm.valueConverters.stringToBoolean,
            booleanArray    : AutoForm.valueConverters.stringToBooleanArray,
            date            : AutoForm.valueConverters.stringToDate,
            dateArray       : AutoForm.valueConverters.stringToDateArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.maxlength === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.maxlength = newContext.max
            }
            return newContext
        }
    })

    AutoForm.addInputType('numberProyecto', {
        template: 'afProyectoInputNumber',
        valueOut () {
            // return AutoForm.valueConverters.stringToNumber(this.val());
            return this.val() !== '' ? AutoNumeric.getAutoNumericElement(this.get(0)).getNumber() : null
        },
        valueConverters: {
            string          : AutoForm.valueConverters.numberToString,
            stringArray     : AutoForm.valueConverters.numberToStringArray,
            numberArray     : AutoForm.valueConverters.numberToNumberArray,
            boolean         : AutoForm.valueConverters.numberToBoolean,
            booleanArray    : AutoForm.valueConverters.numberToBooleanArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.max === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.max = newContext.max
            }
            if (typeof newContext.atts.min === 'undefined' && typeof newContext.min === 'number') {
                newContext.atts.min = newContext.min
            }
            return newContext
        }
    })

    AutoForm.addInputType('textareaProyecto', {
        template        : 'afProyectoTextarea',
        valueConverters : {
            stringArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    return linesToArray(val)
                }
                return val
            },
            number      : AutoForm.valueConverters.stringToNumber,
            numberArray : AutoForm.valueConverters.stringToNumberArray,
            boolean     : AutoForm.valueConverters.stringToBoolean,
            booleanArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    const arr = linesToArray(val)
                    return arr.map(item => AutoForm.valueConverters.stringToBoolean(item))
                }
                return val
            },
            date: AutoForm.valueConverters.stringToDate,
            dateArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    const arr = linesToArray(val)
                    return arr.map(item => AutoForm.valueConverters.stringToDate(item))
                }
                return val
            }
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.maxlength === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.maxlength = newContext.max
            }
            return newContext
        }
    })

    AutoForm.addInputType('textareaWYSIWYGProyecto', {
        template: 'afProyectoTextareaWYSIWYG',
        valueOut () {
            return this.code()
        },
        valueConverters: {
            stringArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    return linesToArray(val)
                }
                return val
            },
            number      : AutoForm.valueConverters.stringToNumber,
            numberArray : AutoForm.valueConverters.stringToNumberArray,
            boolean     : AutoForm.valueConverters.stringToBoolean,
            booleanArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    const arr = linesToArray(val)
                    return arr.map(item => AutoForm.valueConverters.stringToBoolean(item))
                }
                return val
            },
            date: AutoForm.valueConverters.stringToDate,
            dateArray (val) {
                if (typeof val === 'string' && val.length > 0) {
                    const arr = linesToArray(val)
                    return arr.map(item => AutoForm.valueConverters.stringToDate(item))
                }
                return val
            }
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.maxlength === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.maxlength = newContext.max
            }
            return newContext
        }
    })

    AutoForm.addInputType('selectProyecto', {
        template: 'afProyectoSelect',
        valueOut () {
            return this.val()
        },
        valueConverters: {
            stringArray     : AutoForm.valueConverters.stringToStringArray,
            number          : AutoForm.valueConverters.stringToNumber,
            numberArray     : AutoForm.valueConverters.stringToNumberArray,
            boolean         : AutoForm.valueConverters.stringToBoolean,
            booleanArray    : AutoForm.valueConverters.stringToBooleanArray,
            date            : AutoForm.valueConverters.stringToDate,
            dateArray       : AutoForm.valueConverters.stringToDateArray
        },
        contextAdjust (context) {
            const newContext = context
            // can fix issues with some browsers selecting the firstOption instead of the selected option
            newContext.atts.autocomplete = 'off'

            const itemAtts = _.omit(newContext.atts, 'firstOption')
            const { firstOption } = newContext.atts

            // build items list
            newContext.items = []

            // If a firstOption was provided, add that to the items list first
            if (firstOption !== false) {
                newContext.items.push({
                    name     : newContext.name,
                    label    : typeof firstOption === 'string' ? firstOption : '(Select One)',
                    value    : '',
                    // _id must be included because it is a special property that
                    // #each uses to track unique list items when adding and removing them
                    // See https://github.com/meteor/meteor/issues/2174
                    //
                    // Setting this to an empty string caused problems if option with value
                    // 1 was in the options list because Spacebars evaluates "" to 1 and
                    // considers that a duplicate.
                    // See https://github.com/aldeed/meteor-autoform/issues/656
                    _id      : 'AUTOFORM_EMPTY_FIRST_OPTION',
                    selected : false,
                    atts     : itemAtts
                })
            }

            // Add all defined options
            newContext.selectOptions.forEach(opt => {
                if (opt.optgroup) {
                    const subItems = opt.options.map(subOpt => ({
                        name     : newContext.name,
                        label    : subOpt.label,
                        value    : subOpt.value,
                        htmlAtts : _.omit(subOpt, ['label', 'value']),
                        // _id must be included because it is a special property that
                        // #each uses to track unique list items when adding and removing them
                        // See https://github.com/meteor/meteor/issues/2174
                        //
                        // The toString() is necessary because otherwise Spacebars evaluates
                        // any string to 1 if the other values are numbers, and then considers
                        // that a duplicate.
                        // See https://github.com/aldeed/meteor-autoform/issues/656
                        _id      : subOpt.value.toString(),
                        selected : subOpt.value === newContext.value,
                        atts     : itemAtts
                    }))
                    newContext.items.push({
                        optgroup : opt.optgroup,
                        items    : subItems
                    })
                } else {
                    newContext.items.push({
                        name     : newContext.name,
                        label    : opt.label,
                        value    : opt.value,
                        htmlAtts : _.omit(opt, ['label', 'value']),
                        // _id must be included because it is a special property that
                        // #each uses to track unique list items when adding and removing them
                        // See https://github.com/meteor/meteor/issues/2174
                        //
                        // The toString() is necessary because otherwise Spacebars evaluates
                        // any string to 1 if the other values are numbers, and then considers
                        // that a duplicate.
                        // See https://github.com/aldeed/meteor-autoform/issues/656
                        _id      : opt.value.toString(),
                        selected : opt.value === newContext.value,
                        atts     : itemAtts
                    })
                }
            })

            return newContext
        }
    })

    AutoForm.addInputType('imageProyecto', {
        template: 'afProyectoImage'
    })

    AutoForm.addInputType('fileProyecto', {
        template: 'afProyectoFileInput'
    })

    AutoForm.addInputType('dateProyecto', {
        template: 'afProyectoDate',
        valueIn (val) {
            // convert Date to string value
            return AutoForm.valueConverters.dateToDateStringUTC(val)
        },
        valueOut () {
            const val = new Date(this.val())
            if (val.toString() !== 'Invalid Date') {
                return val
            }
            return null

            // if (AutoForm.Utility.isValidDateString(val)) {
            // 	//Date constructor will interpret val as UTC and create
            // 	//date at mignight in the morning of val date in UTC time zone
            // 	return new Date(val);
            // } else {
            // 	return null;
            // }
        },
        valueConverters: {
            string      : AutoForm.valueConverters.dateToDateStringUTC,
            stringArray : AutoForm.valueConverters.dateToDateStringUTCArray,
            number      : AutoForm.valueConverters.dateToNumber,
            numberArray : AutoForm.valueConverters.dateToNumberArray,
            dateArray   : AutoForm.valueConverters.dateToDateArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.max === 'undefined' && newContext.max instanceof Date) {
                newContext.atts.max = AutoForm.valueConverters.dateToDateStringUTC(newContext.max)
            }
            if (typeof newContext.atts.min === 'undefined' && newContext.min instanceof Date) {
                newContext.atts.min = AutoForm.valueConverters.dateToDateStringUTC(newContext.min)
            }
            return newContext
        }
    })

    AutoForm.addInputType('dateTimeProyecto', {
        template: 'afProyectoDateTime',
        valueIn (val) {
            // convert Date to string value
            const d = moment(val)
            return d.format('YYYY-MM-DD hh:mm')
        },
        valueOut () {
            const val = new Date(this.val())
            if (val.toString() !== 'Invalid Date') {
                return val
            }
            return null

            // if (AutoForm.Utility.isValidDateString(val)) {
            // 	//Date constructor will interpret val as UTC and create
            // 	//date at mignight in the morning of val date in UTC time zone
            // 	return new Date(val);
            // } else {
            // 	return null;
            // }
        },
        valueConverters: {
            string      : AutoForm.valueConverters.dateToDateStringUTC,
            stringArray : AutoForm.valueConverters.dateToDateStringUTCArray,
            number      : AutoForm.valueConverters.dateToNumber,
            numberArray : AutoForm.valueConverters.dateToNumberArray,
            dateArray   : AutoForm.valueConverters.dateToDateArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.max === 'undefined' && newContext.max instanceof Date) {
                newContext.atts.max = AutoForm.valueConverters.dateToDateStringUTC(newContext.max)
            }
            if (typeof newContext.atts.min === 'undefined' && newContext.min instanceof Date) {
                newContext.atts.min = AutoForm.valueConverters.dateToDateStringUTC(newContext.min)
            }
            return newContext
        }
    })

    AutoForm.addInputType('increaseDecreaseProyecto', {
        template: 'afProyectoIncreaseDecrease',
        valueOut () {
            const elem = this[0],
                schema = AutoForm.getSchemaForField(elem.name),
                { min, max } = schema,
                val = AutoForm.valueConverters.stringToNumber(this.val())
            if (val >= min && val <= max) {
                return val
            }
            return null
        },
        valueConverters: {
            string       : AutoForm.valueConverters.numberToString,
            stringArray    : AutoForm.valueConverters.numberToStringArray,
            numberArray  : AutoForm.valueConverters.numberToNumberArray,
            boolean      : AutoForm.valueConverters.numberToBoolean,
            booleanArray : AutoForm.valueConverters.numberToBooleanArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.max === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.max = newContext.max
            }
            if (typeof newContext.atts.min === 'undefined' && typeof newContext.min === 'number') {
                newContext.atts.min = newContext.min
            }
            return newContext
        }
    })

    AutoForm.addInputType('checkBoxProyecto', {
        template: 'afCheckBox',
        valueOut () {
            return $(this).prop('checked')
        },
        contextAdjust (context) {
            const newContext = context
            if (newContext.value) {
                newContext.checked = 'checked'
            } else {
                newContext.checked = ''
            }
            return newContext
        }
    })

    AutoForm.addInputType('switchProyecto', {
        template: 'afSwitch2',
        valueOut () {
            return $(this).prop('checked')
        },
        contextAdjust (context) {
            const newContext = context
            if (newContext.value) {
                newContext.checked = 'checked'
            } else {
                newContext.checked = ''
            }
            return newContext
        }
    })

    AutoForm.addInputType('colorProyecto', {
        template: 'afColor',
        valueOut () {
            return $(this).asColorPicker('get').toHEX()
        }
    })

    AutoForm.addInputType('passwordProyecto', {
        template: 'afProyectoPassInput',
        valueOut () {
            const elem = this[0],
                { form } = elem,
                pass = form.querySelector(`.autoform-${elem.dataset.schemaKey}`).value,
                confirm = form.querySelector(`.autoform-${elem.dataset.schemaKey}.pass-confirm`).value
            if (pass !== confirm) {
                return null
            }
            return this.val()
        },
        valueConverters: {
            stringArray: AutoForm.valueConverters.stringToStringArray
        },
        contextAdjust (context) {
            const newContext = context
            if (typeof newContext.atts.maxlength === 'undefined' && typeof newContext.max === 'number') {
                newContext.atts.maxlength = newContext.max
            }
            return newContext
        }
    })
})

Template.afColor.onRendered(function render () {
    const instance = this
    const el = instance.find('.proyecto-input-container')
    $.asColorPicker.setDefaults({
        hideInput : true,
        mode      : 'complex'
    })
    $('.proyecto-color-picker').asColorPicker()
    el.querySelector('.asColorPicker-trigger > span').classList.add('z-depth-1')
})

Template.afProyectoTextareaWYSIWYG.onRendered(function render () {
    const instance = this
    const el = instance.find('.proyecto-input-container .wysiwyg-style')
    // Contenido del WYSIWYG:
    const toolbar = [
        ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
        ['fonts', ['fontsize', 'fontname']],
        ['color', ['color']],
        ['undo', ['undo', 'redo']],
        ['misc', ['link', 'table', 'hr', 'codeview', 'fullscreen']],
        ['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']],
        ['height', ['lineheight']]
    ]
    $(el).materialnote({
        toolbar,
        height           : 250,
        minHeight        : 100,
        defaultBackColor : '#e0e0e0'
    })
    $(el).siblings('.note-editor')
        .find('button')
        .attr('type', 'button')
    Meteor.defer(() => {
        $(el).code(instance.data.value)
    })
    // Fin del contenido del WYSIWYG
})

Template.afProyectoDateTime.onRendered(function render () {
    const instance = this
    const input = instance.find('.datetimepicker')
    const picker = new MaterialDateTimePicker({
        container: instance.$('.datetimepicker-holder').parent().parent()[0]
    }).on('submit', val => {
        input.value = val.format('YYYY-MM-DD HH:mm')
    })
    Meteor.defer(() => {
        input.addEventListener('focus', () => picker.open() || picker.set(moment()))
    })
    instance.picker = picker
})

Template.afProyectoDateTime.events({
    'focus input' (e, instance) {
        const modal = window.findParentContext('Template.proyectoModal', instance.view)
        if (modal) {
            if (modal.find('.proyecto-modal-content').clientHeight < 615) {
                modal.find('.proyecto-modal-content').style.height = '620px'
            }
        }
    }
})

Template.afProyectoInput.onRendered(function render () {
    const instance = this
    if (instance.data.atts.inputMask) {
        Meteor.defer(() => {
            Inputmask(instance.data.atts.inputMask).mask(instance.find('input.proyecto-input'))
        })
    }
})

Template.afProyectoInputNumber.onRendered(function render () {
    const instance = this
    Meteor.defer(() => {
        new AutoNumeric(instance.find('input.proyecto-autonumeric'), instance.data.atts.aN) // eslint-disable-line no-new
    })
})

Template.afProyectoSelect.onCreated(function create () {
    const instance = this,
        { data } = instance,
        opts = data.atts
    if (opts.dependsOn) {
        const key = `change [data-schema-key="${opts.dependsOn.key}"]`,
            map = {},
            query = {},
            modal = window.findParentContext('Template.proyectoModal', instance.view)
        map[key] = e => {
            query[`${opts.dependsOn.query}`] = e.target.value
            instance[`sub_${opts.dependsOn.key}`] = instance.subscribe('simplePublish', opts.dependsOn.collection, query)
            instance.autorun(computation => {
                const vals = Mongo.Collection.get(opts.dependsOn.collection).find(query).fetch(),
                    options = _.clone(modal[opts.dependsOn.var]())
                if (instance[`sub_${opts.dependsOn.key}`].ready()) {
                    options.splice(1)
                    vals.forEach(i => {
                        options.push({ value: i[opts.dependsOn.value], label: i[opts.dependsOn.label] })
                    })
                    modal[opts.dependsOn.var](options)
                    refreshSelect(instance)
                    computation.stop()
                }
            })
        }
        _.assignIn(instance.view.template.__eventMaps[0], map)
    }
})

Template.afProyectoSelect.onRendered(function render () {
    const instance = this
    Meteor.defer(() => {
        refreshSelect(instance)
    })
})

Template.afProyectoSelect.events({
    'focus input' (e, instance) {
        Meteor.setTimeout(() => {
            if (instance.find('ul.select-dropdown')) {
                instance.find('ul.select-dropdown').scrollIntoViewIfNeeded()
                // instance.find('ul.select-dropdown').scrollIntoView({
                // 	behavior: "instant",
                // 	block: "start",
                // })
            }
        }, 350)
    }
})

Template.afProyectoImage.onCreated(function create () {
    const instance = this
    instance.image = new ReactiveField(false)
    instance.editImage = new ReactiveField(false)
    if (instance.data.value) {
        instance.image(true)
        instance.editImage(true)
    }
})

Template.afProyectoImage.events({
    'click .proyecto-file-container' (e, instance) {
        instance.find('input[type=file]').click()
    },
    'change input.proyecto-image-input[type=file]' (e, instance) {
        if (!e.currentTarget.files.length) {
            instance.image(false)
            instance.editImage(false)
        } else {
            instance.image(true)
            Meteor.defer(() => {
                window.imageReader(e.currentTarget, instance.find('img.proyecto-image-preview'))
            })
        }
    }
})

Template.afProyectoFileInput.events({
    'click .proyecto-file-button' (e, instance) {
        e.preventDefault()
        instance.find('input[type="file"]').click()
    },
    'change input[type="file"]' (e, instance) {
        const file = e.currentTarget.files,
            self = instance
        if (!file.length) {
            self.find('input.proyecto-file-input').value = ''
        } else {
            self.find('input.proyecto-file-input').value = file[0].name
        }
    }
})

Template.afProyectoDate.onRendered(function render () {
    const instance = this
    Meteor.defer(() => {
        $(instance.find('input.datepicker')).pickadate(instance.data.atts.picker)
    })
})

Template.afProyectoDate.events({
    'change input.datepicker' (e, instance) {
        const hidden = instance.find('input[type=hidden]'),
            real = instance.find('input.content-hide')
        real.value = new Date(hidden.value)
    },
    'focus input' (e, instance) {
        const modal = window.findParentContext('Template.proyectoModal', instance.view)
        if (modal) {
            if (modal.find('.proyecto-modal-content').clientHeight < 615) {
                modal.find('.proyecto-modal-content').style.height = '675px'
            }
        }
    }
})

Template.afProyectoIncreaseDecrease.onRendered(function render () {
    const instance = this,
        elem = instance.find('input.proyecto-increase-decrease')
    elem.value = !instance.data.value ? 0 : instance.data.value
    Meteor.defer(() => {
        new AutoNumeric(elem, instance.data.atts.aN) // eslint-disable-line no-new
    })
})

Template.afProyectoIncreaseDecrease.events({
    'click .decrease-btn' (e, instance) {
        e.preventDefault()
        const { min } = this.atts,
            elem = instance.find('input.proyecto-increase-decrease'),
            numeric = AutoNumeric.getAutoNumericElement(elem)
        if (numeric.getNumber() <= min) {
            Materialize.newToast('No es posible reducir el valor', 3500, 'toast-negative')
        } else {
            numeric.set(numeric.getNumber() - 1)
        }
    },
    'click .increase-btn' (e, instance) {
        e.preventDefault()
        const { max } = this.atts,
            elem = instance.find('input.proyecto-increase-decrease'),
            numeric = AutoNumeric.getAutoNumericElement(elem)
        if (numeric.getNumber() >= max) {
            Materialize.newToast('No es posible aumentar el valor', 3500, 'toast-negative')
        } else {
            numeric.set(numeric.getNumber() + 1)
        }
    }
})

Template.afProyectoPassInput.events({
    'input input[type=password]': _.debounce((e, instance) => {
        const pass = instance.find('input:not(.pass-confirm)'),
            confirm = instance.find('input.pass-confirm')
        if (pass.value === confirm.value) {
            const arr = instance.findAll('.proyecto-not-valid-color')
            arr.forEach(i => {
                i.classList.remove('proyecto-not-valid-color')
            })
        }
    }, 250)
})
